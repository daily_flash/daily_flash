/*Create a Container in the Center of the screen, now In the background of
the Container display an Image (the image can be an asset or network
image ). Also, display text in the center of the Container.
*/
import 'package:flutter/material.dart';
class Program2 extends StatelessWidget{
  const Program2({super.key});
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: const Text("Program1"),
      centerTitle: true,),
      body: Center(
        child: Container(
          width: 300,
          height: 300, 
          alignment: Alignment.center,
          decoration: const BoxDecoration(image: DecorationImage(image:NetworkImage("https://exchange4media.gumlet.io/news-photo/128136-main.jpg?w=400&dpr=2.6",),fit: BoxFit.cover ),),
          child:const Padding(

            padding:  EdgeInsets.all(15.0),
            child:  Text("Smriti Mandhana",style: TextStyle(color: Colors.red,fontSize: 40,fontWeight: FontWeight.bold),),
          )
        ),
      ),
    );
  }
}
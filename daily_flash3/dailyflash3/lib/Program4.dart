/*
Create a Container with size(height:200, width:300) now give a shadow to
the container but the shadow must only be at the top side of the container.
*/
import 'package:flutter/material.dart';
class Program4 extends StatelessWidget{
  const Program4({super.key});
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: const Text("Program1"),
      centerTitle: true,),
      body: Center(
        child: Container(
          width: 300,
          height: 300, 
          decoration: const BoxDecoration(color: Colors.amber,boxShadow: [BoxShadow(color: Colors.red,blurRadius: 20,offset: Offset(0, -32))]),
         
          
        ),
      ),
    );
  }
}
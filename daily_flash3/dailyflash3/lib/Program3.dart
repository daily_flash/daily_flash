/*
Add a container in the center of the screen with a size(width: 200,
height: 200). Give a red border to the container. Now when the user taps
the container change the color of the border to green.
*/
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Program3 extends StatefulWidget{
  const Program3({super.key});
  @override
  State createState()=>_Program3();
}
class _Program3 extends State{
   bool Colorchange=true;
  Widget build(BuildContext context){
   
    return Scaffold(
      appBar:AppBar(
        title: const Text("Program 3"),
        centerTitle: true,
      ) ,
      body:Center(
        child: GestureDetector(
          onTap: (){
              setState(() {
                Colorchange=!Colorchange;
              });
          },
          child: Container(
            width: 200,
            height: 200, 
           
            decoration: BoxDecoration(border: Border.all(width: 10,color: (Colorchange)?Colors.red:Colors.green)),
            child:const Padding(
        
              padding:  EdgeInsets.all(15.0),
             
            )
          ),
        ),
      ) ,
    );
  }
}
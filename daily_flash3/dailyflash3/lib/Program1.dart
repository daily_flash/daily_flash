import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
/*
1. Create a Container in the Center of the Screen with size(width: 300,
height: 300) and display an image in the center of the Container. Apply
appropriate padding to the container.
*/ 
class Program1 extends StatelessWidget{
  const Program1({super.key});
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: const Text("Program1"),
      centerTitle: true,),
      body: Center(
        child: Container(
          width: 300,
          height: 300, 
          color: Colors.black,
          padding: const EdgeInsets.all(20),
          child:Image.network("https://akm-img-a-in.tosshub.com/indiatoday/images/story/202303/qa_-_mandar_deodhar-sixteen_nine.jpg?VersionId=KYq0xeqBvTX04kyzMIh_cKzC2F8Lr4eY&size=690:388",fit: BoxFit.cover,) ,
        ),
      ),
    );
  }
}